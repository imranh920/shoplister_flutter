
class User {
  String user_email;
  String token;
  String user_nicename;
  String user_display_name;
  String user_channel;
  String user_id;

  User({this.user_display_name, this.token,this.user_email,this.user_id,this.user_nicename,this.user_channel});

  User.map(dynamic obj) {
    this.user_display_name = obj["user_display_name"];
    this.token = obj["token"];
    this.user_email = obj["user_email"];
    this.user_id = obj["user_id"];
    this.user_nicename = obj["user_nicename"];
    this.user_channel=obj["user_channel"];
  }

  String get username => user_display_name;
  String get usertoken => token;
  String get userid => user_id;
  String get useremail => user_email;
  String get usernicename => user_nicename;
  String get userchannel=>user_channel;


  Map<String, dynamic> toJson() => {
        'user_display_name': user_display_name,
        'token': token,
        'user_email': user_email,
        'user_id':user_id,
        'user_nicename':user_nicename,
        'user_channel':user_channel
      };
}
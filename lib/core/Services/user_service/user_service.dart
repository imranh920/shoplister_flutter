import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:shoplister/core/Helper/sharedpref.dart';
import 'package:shoplister/core/Models/user.dart';
import 'package:shoplister/core/Services/network_util.dart';

class UserService {

  NetworkUtil _netUtil = new NetworkUtil();
  SharedPref sharedPref=SharedPref();
  static final BASE_URL = "http://www.abcd.com";
  static final LOGIN_URL = BASE_URL + "/login-user";

  Future<User> login(String username, String password) {
    return () async { return new User(user_email: "imranh920@gmail.com", token: "abcd",user_display_name: "imran",user_nicename: "imran");}();
    return _netUtil.post(LOGIN_URL, body: {
      "user_email": username,
      "user_pass": password
    }).then((dynamic result) {
      try {
        if(result["user_id"]!=null && result["code"]==null){
          return new User.map(result);
        }else if(result["code"]!=null){
          print(result["code"]);
        }
      } catch (e) {
        print(e);
      }
    });
  }
  
  Future<void> closeApp() async {
    await SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
  }

  Future<User> getCurrentUser() async{
    return User.map(await sharedPref.read("currentuser"));
  }

  void setCurrentUser(user){
    sharedPref.save("currentuser", user);
  }

}
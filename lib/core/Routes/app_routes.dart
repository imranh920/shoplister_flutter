
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shoplister/Screens/Views/CustomerSpecificViews/HomeView/customer_home_view.dart';
import 'package:shoplister/Screens/Views/LandingView/landing_view.dart';
import 'package:shoplister/core/Models/user.dart';

const String initialRoute = "login";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/landing':
        return MaterialPageRoute(builder: (_) => LandingView());
      case '/customer_home':
        return MaterialPageRoute(builder: (_) => CustomerHomeView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}

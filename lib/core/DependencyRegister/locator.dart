import 'package:shoplister/ViewModels/BaseModel/base_model.dart';
import 'package:shoplister/ViewModels/CustomerHomeModel/customer_home_model.dart';
import 'package:shoplister/ViewModels/LoginModels/login_model.dart';
import 'package:shoplister/core/Services/auth_service/auth_service.dart';
import 'package:shoplister/core/Services/user_service/user_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  //services
  locator.registerLazySingleton(() => UserService());
  //models
  locator.registerFactory(() => BaseModel());
  locator.registerFactory(() => CustomerHomeModel());
}

import 'package:flutter/material.dart';

const headerStyle = TextStyle(fontSize: 20);
const subHeaderStyle = TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500);
const double fontsizeLarge = 20;
const double fontsizeMedium = 17;
const double fontsizeSmall = 14;

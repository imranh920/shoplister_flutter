import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:shoplister/global_variables/global_vals.dart';

class LandingView extends StatefulWidget {
  @override
  _LandingViewState createState() => new _LandingViewState();
}

class _LandingViewState extends State<LandingView> {
  TextEditingController _serverIpController = TextEditingController();
  TextEditingController _serverPortController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _serverIpController.text = GlobalVals.SERVER_IP;
    _serverPortController.text = GlobalVals.SERVER_PORT;
  }
  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery
        .of(context)
        .size
        .width;
    double screen_height = MediaQuery
        .of(context)
        .size
        .height;
    return 
    Material(child: Container(decoration: BoxDecoration(image: DecorationImage(image: AssetImage("lib/assets/images/grocery.png",), fit: BoxFit.cover),), 
    child: Center(
        child: Column(children: <Widget>[
           SizedBox(height: screen_height / 2.5,),
           TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                filled: true,
                fillColor: Colors.white,
                hintText: 'Server IP',
                labelText: "Server IP:"
              ),
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold,),
              controller: _serverIpController,
            ),
            SizedBox(height: 10,),
            TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                filled: true,
                fillColor: Colors.white,
                hintText: 'Server Port',
                labelText: "Port:",
              ),
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold,
               ),
              controller: _serverPortController,
            ),
            SizedBox(height: 10,),
           InkWell(
          child: Container(
            height: 80, 
            width: 140, 
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.amber.shade500),
             
            child: Center(
              child:  Text("Start App", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold,),),),), 
              radius: 20,
            onTap: () async {
              GlobalVals.SERVER_IP = _serverIpController.text;
              GlobalVals.SERVER_PORT = _serverPortController.text;
              DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
              AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
              GlobalVals.DEVICE_ID = androidInfo.androidId;
              Navigator.pushNamed(context, "/customer_home");
            },
            ),
            ],),
            ),
            ),
            );
  }
}
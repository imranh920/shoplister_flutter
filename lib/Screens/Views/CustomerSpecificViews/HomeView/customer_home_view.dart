
import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shoplister/Screens/Views/BaseView/base_view.dart';
import 'package:shoplister/Screens/Views/CameraView/camera_view.dart';
import 'package:shoplister/Screens/Widgets/badge_icon.dart';
import 'package:shoplister/Screens/Widgets/button.dart';
import 'package:shoplister/ViewModels/CustomerHomeModel/customer_home_model.dart';
import 'package:shoplister/core/Models/user.dart';

class CustomerHomeView extends StatefulWidget {

  CustomerHomeView();

  @override
  _CustomerHomeViewState createState() => new _CustomerHomeViewState();
}

class _CustomerHomeViewState extends State<CustomerHomeView> {
  _CustomerHomeViewState();
  final _textController = TextEditingController();
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery
        .of(context)
        .size
        .width;
    double screen_height = MediaQuery
        .of(context)
        .size
        .height;
    return BaseView<CustomerHomeModel>(
      builder: (context, model, child) =>
        WillPopScope(
          onWillPop: () {},
          child: Scaffold(
            appBar: AppBar(
              title: Text('Home'),
            ),
            drawer: Drawer(child: ListView(padding: EdgeInsets.zero, children: <Widget>[
              _createHeader(screen_height),
            ],)),
            body: Material(child: 
            Stack(children: <Widget>[
              Positioned.fill(
              child:Container(
                decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("lib/assets/images/grocery.png"), fit: BoxFit.cover,)),
                child: Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 0.5),
                  ),
                  child: 
                ListView(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 10,),
                          Row(children: <Widget>[

                          _createImageContainer(screen_height, screen_width, model.image1, (imgPath){ model.image1 = imgPath; }),
                          SizedBox(height: 10,),
                          _createImageContainer(screen_height, screen_width, model.image2, (imgPath){ model.image2 = imgPath; }),
                          
                          ],),
                          SizedBox(height: 10,),
                          _createImageContainer(screen_height, screen_width, model.image3, (imgPath){ model.image3 = imgPath; }),
                          SizedBox(height: 10,),

                        ],
                      ),
                    ),
                    Container(decoration: BoxDecoration(color: Colors.white70, border: Border.all(color: Colors.black87)),
                    child: TextField(controller: _textController, cursorWidth: 4, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40), 
                    decoration: new InputDecoration(labelText: "Enter Value", fillColor: Colors.blue),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
            ]),),
                    
                  ],
                ),
              )
                ,)  ,
            )
            ],)
            ,), 
            bottomNavigationBar:
            Material(child: 
            Row(mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: InkWell(child: Container(height: 40, child: Center(child: Icon(Icons.clear)),), onTap: (){ setState(() {
                  model.image1 = model.image2 = model.image3 = "";
                  model.valueText = "0";
                }); },),
              ),
              Expanded(
                flex: 1,
                child: InkWell(child: Container(height: 40, child: Center(child: Icon(Icons.cloud_upload)),),
                onTap: () async { 
                  model.valueText = _textController.text;
                  await model.saveImage();
                  setState(() {
                    model.image1 = model.image2 = model.image3 = "";
                    _textController.text = "0";  
                    model.valueText = "0";
                  });
                },),
              )
            ],)
            ,)
            ,
          ),
        )
    );
  }
  @override
  void dispose() {
    super.dispose();
  }
  Widget _createImageContainer(screen_height, screen_width, img, callback){
    return InkWell(child: 
      Container(height: screen_height / 4, width: screen_width/2, decoration: BoxDecoration(
        border: Border.all(color: Colors.blueAccent),
        image: DecorationImage(image: 
          img == "" ? AssetImage("lib/assets/logo/productPlaceholder.png") : FileImage(File(img)),fit: BoxFit.cover
        ),
      ),)
      ,
      onTap: () async {
        String imgPath = await Navigator.push(context, MaterialPageRoute(builder: (context){ return CameraView();}));
        setState(() {
          callback(imgPath);  
        });
        
      },
    );
  }
  Widget _createHeader(screen_height) {
    return Container(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        height: screen_height*0.35,
        color: Colors.amber[300],
        child: Stack(children: <Widget>[
          Align(
            child:Container(
                child: Icon(Icons.account_circle,color: Colors.grey,size: 100.0,)
            ),
          ),
          Positioned(
              bottom: 12.0,
              left: 60.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("User",
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500)),
                  SizedBox(height: 5.0,),
                  Text("",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          ))
                ],
              )
            ),
        ]));
  }
}
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class CameraView extends StatefulWidget {

  CameraView();

  @override
  _CameraViewState createState() => new _CameraViewState();
}
class _CameraViewState extends State {
  _CameraViewState();
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  bool isCameraReady = false;
  bool showCapturedPhoto = false;
  var ImagePath;
  void initState() {
    // TODO: implement initState
    super.initState();
    _initializeCamera();
  }
  Future<void> _initializeCamera() async {
    final cameras = await availableCameras();
    final firstCamera = cameras.first;
    _controller = CameraController(firstCamera,ResolutionPreset.high);
    _initializeControllerFuture = _controller.initialize();
    if (!mounted) {
      return;
    }
    setState(() {
      isCameraReady = true;
    });
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;
    return Scaffold(body: 
    FutureBuilder<void>(
  future: _initializeControllerFuture,
  builder: (context, snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      // If the Future is complete, display the preview.
      return Transform.scale(
          scale: _controller.value.aspectRatio / deviceRatio,
          child: Center(
            child: AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: CameraPreview(_controller), //cameraPreview
            ),
          ));
    } else {
      return Center(
          child:
              CircularProgressIndicator()); // Otherwise, display a loading indicator.
    }
  },
),
floatingActionButton: FloatingActionButton(child: Icon(Icons.camera), onPressed: () async {
  String imagePath = await onCaptureButtonPressed();
  Navigator.pop(context, imagePath);
},)
    ,); 
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _controller != null
          ? _initializeControllerFuture = _controller.initialize()
          : null; //on pause camera is disposed, so we need to call again "issue is only for android"
    }
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  Future<String> onCaptureButtonPressed() async {  //on camera button press
    try {

      final path = join(
        (await getTemporaryDirectory()).path, //Temporary path
        '${DateFormat("yyyy-MM-dd-HH-mm-ss").format(DateTime.now())}.png',
      );
      ImagePath = path;
      await _controller.takePicture(path); //take photo
      return path;
      setState(() {
        showCapturedPhoto = true;
      });
    } catch (e) {
      print(e);
    }
  }
}
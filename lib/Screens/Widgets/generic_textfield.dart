import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shoplister/core/Helper/validators.dart';

class GenericTextField extends StatefulWidget{
  TextEditingController _textController;
  String hintText;
  var validators;

  var icon;
  GenericTextField(this._textController,this.hintText,this.icon,{this.validators});

  @override
  _GenericTextFieldState createState(){
    return new  _GenericTextFieldState(_textController,hintText,icon,validators);
  }

}

class _GenericTextFieldState extends State<GenericTextField>{

  TextEditingController _textController;
  String hintText;
  var validators;
  var icon;
  _GenericTextFieldState(this._textController,this.hintText,this.icon,this.validators);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:TextFormField(
        controller: _textController,
        style: TextStyle(color: Colors.blue.shade900),
        validator: this.validators,
        decoration: InputDecoration(
          prefixIcon: Icon(
            this.icon,
            color: Colors.blue.shade900,
          ),
          hintText: this.hintText,
          hintStyle: TextStyle(color: Colors.blue.shade900),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.blue, width: 0.5),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.blue, width: 2.0),
          ),
      ),
    ));
  }
}
import 'dart:io';
import 'package:flutter/material.dart';
import 'core/DependencyRegister/locator.dart';
import 'core/Routes/app_routes.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ShopLister',
      theme: ThemeData(
        primaryColor: Colors.amber.shade300,
      ),
      initialRoute: '/landing',
      onGenerateRoute: Router.generateRoute,
    );
  }
}


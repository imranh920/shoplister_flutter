import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:shoplister/ViewModels/BaseModel/base_model.dart';
import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'package:intl/intl.dart';
import 'package:shoplister/global_variables/global_vals.dart';


class CustomerHomeModel extends BaseModel{
  
  StreamController<int> notificationCountController = StreamController<int>.broadcast();
  int notificationCount=1;
  String image1 = "";
  String image2 = "";
  String image3 = "";
  String uploadURL = "http://${GlobalVals.SERVER_IP}:${GlobalVals.SERVER_PORT}/home/saveshopitem";
  String valueText = "";
  Upload(List<File> imageFiles) async {    
    
      
     var uri = Uri.parse(uploadURL);
     var request = new http.MultipartRequest("POST", uri);
     
     request.fields["Name"] = DateFormat("yyyy-MM-dd-HH-mm-ss").format(DateTime.now());
     request.fields["Price"] = valueText;
     request.fields["DeviceId"] = GlobalVals.DEVICE_ID;
     
     int i = 1;
     imageFiles.forEach( await (f) async {
      var length = await f.length();  
       var stream = new http.ByteStream(DelegatingStream.typed(f.openRead()));
           
       var multipartFile = new http.MultipartFile('Image' + i.toString(), stream, length,
          filename: path.basename(f.path));
          //contentType: new MediaType('image', 'png'));
          request.files.add(multipartFile);
        i++;
     });
     
      var response = await request.send();
      print(response.statusCode);
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);
      });
    }
    void saveImage() async {
      //final directory = await getExternalStorageDirectory();
      //DateFormat dateFormat = DateFormat("yyyy-MM-dd-HH-mm-ss");
      //final myImagePath = '${directory.path}/ShopLister/${dateFormat.format(DateTime.now())}' ;
      //final myImgDir = await new Directory(myImagePath).create(recursive: true);

      List<File> imageFiles = List<File>();
      if(image1 != ""){
        var file1 = File.fromUri(Uri(path: image1));
        imageFiles.add(file1);
        //await file1.copy(myImagePath + "/" + path.basename(file1.path));
        //await file1.delete();
      }
      if(image2 != ""){
        var file1 = File.fromUri(Uri(path: image2));
        imageFiles.add(file1);
        //await file1.copy(myImagePath + "/" + path.basename(file1.path));
        //await file1.delete();
      }
      if(image3 != ""){
        var file1 = File.fromUri(Uri(path: image3));
        imageFiles.add(file1);
        //await file1.copy(myImagePath + "/" + path.basename(file1.path));
        //await file1.delete();
      }
      await Upload(imageFiles);
      imageFiles.forEach(await (s) async {
        await s.delete();
      });
    }
}